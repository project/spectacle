<?php
/**
 * @file
 * spectacle_config.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function spectacle_config_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function spectacle_config_node_info() {
  $items = array(
    'branding' => array(
      'name' => t('Branding'),
      'base' => 'node_content',
      'description' => t('Provides default brand assets for layouts'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'content_group' => array(
      'name' => t('Content Group'),
      'base' => 'node_content',
      'description' => t('Use <em>content groups</em> for defining content containers, which fit within digital signage layouts.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'display' => array(
      'name' => t('Display'),
      'base' => 'node_content',
      'description' => t('Use <em>displays</em> for defining digital signage endpoints.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'layout' => array(
      'name' => t('Layout'),
      'base' => 'node_content',
      'description' => t('Use <em>layouts</em> for defining digital signage region layouts.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'post' => array(
      'name' => t('Post'),
      'base' => 'node_content',
      'description' => t('Use <em>posts</em> for adding digital signage content.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
