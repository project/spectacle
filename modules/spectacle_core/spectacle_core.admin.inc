<?php

/**
 * @file
 * Spectacle Core admin settings.
 */

/**
 * Admin form for Spectacle onfiguration.
 */
function spectacle_core_admin_form($form, &$form_state) {
  $form['spectacle_core_meteor_socket'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('spectacle_core_meteor_socket', 'http://localhost:3000/sockjs'),
    '#description' => t('Enter the /sockjs address of your Meteor application. This is usually the URL to your Meteor application followed by /sockjs.'),
    '#title' => t('SockJS path for Meteor application'),
  );

  return system_settings_form($form);
}
