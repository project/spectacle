; Drush Make stub file for Spectacle
;
; Use this file to build a full distribution including Drupal core and the
; Spectacle install profile using the following command:
;
; drush make build-spectacle.make <target directory>

api = 2
core = 7.x

; Drupal core
; -----------
includes[] = drupal-org-core.make


; Install profiles
; ----------------
projects[spectacle][type] = profile
projects[spectacle][download][type] = git
projects[spectacle][download][url] = http://git.drupal.org/project/spectacle.git
projects[spectacle][download][branch] = 7.x-1.x
