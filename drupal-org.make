; Drush Make file for Spectacle
api = 2
core = 7.x

; Modules
; -------
projects[color_field][subdir] = contrib
projects[color_field][version] = 1.6

projects[ctools][subdir] = contrib
projects[ctools][version] = 1.4

projects[date][subdir] = contrib
projects[date][version] = 2.8

projects[diff][subdir] = contrib
projects[diff][version] = 3.2

projects[entity][subdir] = contrib
projects[entity][version] = 1.5

projects[entityreference][subdir] = contrib
projects[entityreference][version] = 1.1

projects[features][subdir] = contrib
projects[features][version] = 2.2

projects[jquery_update][subdir] = contrib
projects[jquery_update][version] = 2.4

projects[libraries][subdir] = contrib
projects[libraries][version] = 2.2

projects[nodejs][subdir] = contrib
projects[nodejs][version] = 1.9

projects[pathauto][subdir] = contrib
projects[pathauto][version] = 1.2

projects[scheduler][subdir] = contrib
projects[scheduler][version] = 1.2

projects[strongarm][subdir] = contrib
projects[strongarm][version] = 2.0

projects[token][subdir] = contrib
projects[token][version] = 1.5

projects[video_embed_field][subdir] = contrib
projects[video_embed_field][version] = 2.0-beta8

projects[views][subdir] = contrib
projects[views][version] = 3.8

; Themes
; -------
projects[bootstrap][subdir] = contrib
projects[bootstrap][version] = 3.0

; Libraries
; ---------
libraries[profiler][download][type] = "get"
libraries[profiler][download][url] = "http://ftp.drupal.org/files/projects/profiler-7.x-2.0-beta1.tar.gz"

libraries[color_field][download][type] = "get"
libraries[color_field][download][url] = "http://github.com/bgrins/spectrum/zipball/1.4.1"
libraries[color_field][directory_name] = bgrins-spectrum
